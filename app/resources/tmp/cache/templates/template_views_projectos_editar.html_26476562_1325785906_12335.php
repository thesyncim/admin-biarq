	


<? echo $this->html->style('ui-darkness/jquery-ui-1.8.16.custom'); ?>


<style>
    #gallery { float: left; width: 65%; min-height: 12em; } * html #gallery { height: 12em; } /* IE6 */
    .gallery.custom-state-active { background: #eee; }
    .gallery li { float: left; width: 96px; padding: 0.4em; margin: 0 0.4em 0.4em 0; text-align: center; }
    .gallery li h5 { margin: 0 0 0.4em; cursor: move; }
    .gallery li a { float: right; }
    .gallery li a.ui-icon-zoomin { float: left; }
    .gallery li a.ui-icon-trash2 { float: left; }
    .gallery li img { width: 100%; cursor: move; }

    #trash { float: right; width: 32%; min-height: 18em; padding: 1%;} * html #trash { height: 18em; } /* IE6 */
    #trash h4 { line-height: 16px; margin: 0 0 0.4em; }
    #trash h4 .ui-icon { float: left; }
    #trash .gallery h5 { display: none; }
</style>
<script>
    $(function() {
        // there's the gallery and the trash
        var $gallery = $( "#gallery" ),
        $trash = $( "#trash" );

        // let the gallery items be draggable
        $( "li", $gallery ).draggable({
            cancel: "a.ui-icon", // clicking an icon won't initiate dragging
            revert: "invalid", // when not dropped, the item will revert back to its initial position
            containment: $( "#demo-frame" ).length ? "#demo-frame" : "document", // stick to demo-frame if present
            helper: "clone",
            cursor: "move"
        });

        // let the trash be droppable, accepting the gallery items
        $trash.droppable({
            accept: "#gallery > li",
            activeClass: "ui-state-highlight",
            drop: function( event, ui ) {
                deleteImage( ui.draggable );
            }
        });

        // let the gallery be droppable as well, accepting items from the trash
        $gallery.droppable({
            accept: "#trash li",
            activeClass: "custom-state-active",
            drop: function( event, ui ) {
                recycleImage( ui.draggable );
            }
        });

        // image deletion function
        function deleteImage( $item ) {
           
                   
            $item.fadeOut(function() {
                var $list = $( "ul", $trash ).length ?
                    $( "ul", $trash ) :
                    $( "<ul class='gallery ui-helper-reset'/>" ).appendTo( $trash );
                var url = $item.find( "a.ui-icon-trash" ).attr("href");
                var url2 = $item.find( "a.ui-icon-zoomin" ).attr("href");
                $item.find( "a.ui-icon-trash" ).remove();
                $item.find( "a.ui-icon-zoomin" ).remove();
               
                var refresh_icon = "<a href='"+ url2 +"' title= 'anular' class='ui-icon ui-icon-refresh'>anular</a>";
		             
                var trash_icon = "<a href='"+ url +"' title='Apagar esta mensagem' class='ui-icon ui-icon-trash2'>Apagar imagem</a>";
                
                $item.append(refresh_icon);          
                $item.append( trash_icon ).appendTo( $list ).fadeIn(function() {
                    $item
                    .animate({ width: "48px" })
                    .find( "img" )
                    .animate({ height: "36px" });
                });
                
            });
        }
        function deleteImagereal( $item ) {
                   
            $item.fadeOut(function() {
                
                var $list = $( "ul", $trash ).length ?
                    $( "ul", $trash ) :
                    $( "<ul class='gallery ui-helper-reset'/>" ).appendTo( $trash );
                var url = $item.find( "a.ui-icon-trash" ).attr("href");
                $.get(url);
                $item.find( "a.ui-icon-trash2" ).remove();
                               
                              
				
            });
        }

        // image recycle function
        var trash_icon = "<a href='link/to/trash/script/when/we/have/js/off' title='Delete this image' class='ui-icon ui-icon-trash'>Delete image</a>";
        function recycleImage( $item ) {
           
		
            $item.fadeOut(function() {
                  $item.find( "a.ui-icon-trash2" ).remove();
                  
                var url = $("a.ui-icon-trash2").attr("href");
                var url2 = $("a.ui-icon-refresh").attr("href");
                var zoom = "<a href='"+ url2 +"' title= 'Ver maior' class='ui-icon ui-icon-zoomin'>Ver maior</a>";
                var eliminar = "<a href='"+ url +"' title= 'apagar' class='ui-icon ui-icon-trash'>Apagar</a>";
                 $item.append(eliminar);       
                $item
                .find( "a.ui-icon-refresh" )
                .remove()
                
                .end()
                .css( "width", "96px")
                .append( zoom )
                .find( "img" )
                .css( "height", "72px" )
                .end()
                .appendTo( $gallery )
                .fadeIn();
              
                                        
            });
        }

        // image preview function, demonstrating the ui.dialog used as a modal window
        function viewLargerImage( $link ) {
            
            var src = $link.attr( "href" ),
            title = $link.siblings( "img" ).attr( "alt" ),
            $modal = $( "img[src$='" + src + "']" );

            if ( $modal.length ) {
                $modal.dialog( "open" );
            } else {
                var img = $( "<img alt='" + title + "' width='635' height='381' style='display: none; padding: 8px;' />" )
                .attr( "src", src ).appendTo( "body" );
                setTimeout(function() {
                    img.dialog({
                        title: title,
                        width: 650,
                        modal: true
                    });
                }, 1 );
            }
        }

        // resolve the icons behavior with event delegation
        $( "ul.gallery > li" ).click(function( event ) {
            var $item = $( this ),
            $target = $( event.target );

            if ( $target.is( "a.ui-icon-trash" ) ) {
                deleteImage( $item );
            } else if ( $target.is( "a.ui-icon-zoomin" ) ) {
                viewLargerImage( $target );
            } else if ( $target.is( "a.ui-icon-refresh" ) ) {
                recycleImage( $item );
            } else if ( $target.is( "a.ui-icon-trash2" ) ) {
                deleteImagereal( $item );
            }

            return false;
        });
    });
</script>




<script type="text/javascript">
   
    var i=0;
    function addField(){
        $('div#projectos').append('<?php echo $this->form->field('fotos[]', array('type' => 'file')); ?>');
        i++;
        return false;
    }
</script>
<?php echo $this->html->style(array('imageselect')); ?>
<!-- Load TinyMCE -->
<script type="text/javascript" src="http://admin.biarq.com/js/tiny_mce/jquery.tinymce.js"></script>
<script type="text/javascript" src="http://admin.biarq.com/js/imageselect.js"></script>

<script type="text/javascript">
    $().ready(function() {
        
        $('textarea.tinymce').tinymce({
            // Location of TinyMCE script
            script_url : 'http://admin.biarq.com/js/tiny_mce/tiny_mce.js',

            // General options
            theme : "advanced",
            plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",

            // Theme options
            theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
            theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
            theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
            theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",
            theme_advanced_toolbar_location : "top",
            theme_advanced_toolbar_align : "left",
            theme_advanced_statusbar_location : "bottom",
            theme_advanced_resizing : true

			
        });
    });
</script>
<!-- /TinyMCE -->


<!--  start page-heading -->
<div id="page-heading">
    <h1>editar <?php echo $h($projectos->titulo); ?></h1>
</div>
<!-- end page-heading -->
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
    <tr>
        <th rowspan="3" class="sized"><img src="http://admin.biarq.com/img/shared/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
        <th class="topleft"></th>
        <td id="tbl-border-top">&nbsp;</td>
        <th class="topright"></th>
        <th rowspan="3" class="sized"><img src="http://admin.biarq.com/img/shared/side_shadowright.jpg" width="20" height="300" alt="" /></th>
    </tr>
    <tr>
        <td id="tbl-border-left"></td>
        <td>
            <!--  start content-table-inner ...................................................................... START -->
            <div id="content-table-inner">

                <!--  start table-content  -->
                <div id="table-content">
                    <?php echo $this->_render('element', 'mensagem'); ?>
                    <?php echo $this->form->create($projectos, array('method' => 'post', 'enctype' => 'multipart/form-data')); ?>

                    <?php echo $this->form->field('titulo', array('class' => "inp-form")); ?>
                    <?php echo $this->form->field('texto', array('type' => 'textarea', 'class' => 'tinymce')); ?>
                    <input type="button" onclick="addField();" value="adicionar fotos" /><br/>

                    <div id="projectos"></div>





                    <br>
                    <select name ="fotoprincipal" id="teste">
                        <?php
                        foreach ($projectos->foto as $foto) {
                            $cond = '';
                            if ($projectos->fotoprincipal == $foto)
                                $cond = ' selected="selected"';
                            echo ' <option ' . $cond . ' value="' . $foto . '" > http://admin.biarq.com/img/projectos/pequenas/' . $foto . '</option>';
                        }
                        ?>
                    </select>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>





                    <div class="demo ui-widget ui-helper-clearfix">

                        <ul class="gallery ui-helper-reset ui-helper-clearfix">

                            <?php
                            $r = 1;
                            foreach ($projectos->foto as $foto) {
                                echo'<li class="ui-widget-content ui-corner-tr">
		<h5 class="ui-widget-header">foto' . $r . '</h5>
		<img src="http://admin.biarq.com/img/projectos/pequenas/' . $foto . '"  alt="foto' . $r . '" width="96" height="72" //>
		<a href="http://admin.biarq.com/img/projectos/grandes/' . $foto . '" title="Ver maior" class="ui-icon ui-icon-zoomin">Ver Maior</a>
		<a href="http://admin.biarq.com/projectos/apagarfoto/' . $projectos->_id . '/' . $foto . '" title="Delete this image" class="ui-icon ui-icon-trash">Apagar imagem</a>
	</li>';




                                ++$r;
                            }
                            ?>

                        </ul>

                       

                    </div><!-- End demo -->


                    <script type="text/javascript">

                        $(document).ready(function(){
                            $('select[name=fotoprincipal]').ImageSelect({dropdownWidth:125});
                        });

                    </script>

                    <?php echo $this->form->submit('Editar Projecto'); ?>
                    <?php echo $this->form->end(); ?>
